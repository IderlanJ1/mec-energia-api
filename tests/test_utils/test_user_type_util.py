import pytest
from utils.user.user_type_util import UserType
from users import models

### User types
super_user_type = 'super_user'
university_admin_user_type = 'university_admin'
university_user_type = 'university_user'

all_user_types = [
        super_user_type,
        university_admin_user_type,
        university_user_type
    ]

university_user_types = [
        university_admin_user_type,
        university_user_type
    ]

def test_invalid_user():
    with pytest.raises(Exception):
        UserType.is_valid_user_type('invalid_user')


def test_valid_user():
    try:
        UserType.is_valid_user_type(super_user_type)
    except Exception as e:
        pytest.fail(f"Valid user type failed validation with exception: {e}")


#if user_model == models.UniversityUser and (user_type not in models.CustomUser.university_user_types):
#            raise Exception(f'Wrong User type ({user_type}) for this Model User ({user_model})')

def test_valid_universityUser():
    try:
        UserType.is_valid_user_type(user_model=models.UniversityUser, user_type=models.CustomUser.university_user_type)
    except Exception as e:
        pytest.fail(f"Valid user type failed validation with exception: {e}")

def test_valid_customUser():
    try:
        UserType.is_valid_user_type(user_model=models.CustomUser, user_type=models.CustomUser.super_user_type)
    except Exception as e:
        pytest.fail(f"Valid user type failed validation with exception: {e}")

def test_invalid_universityUser():
    with pytest.raises(Exception):
        UserType.is_valid_user_type(user_model=models.UniversityUser, user_type=models.CustomUser.super_user_type)

def test_invalid_customUser():
    with pytest.raises(Exception):
        UserType.is_valid_user_type(user_model=models.CustomUser, user_type=models.CustomUser.university_user_types)

# get_user_type_by_model

def test_get_user_type_by_model_universityUser():
    assert UserType.get_user_type_by_model(models.UniversityUser) == models.CustomUser.university_user_type

def test_get_user_type_by_model_customUser():
    assert UserType.get_user_type_by_model(models.CustomUser) == models.CustomUser.super_user_type 

def test_get_user_type_by_model_invalid():
    with pytest.raises(Exception):
        UserType.get_user_type_by_model(user_model=models.UserToken)